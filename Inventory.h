#pragma once

#include <list>
#include <utility>
#include <JsonBox.h>

#include "EntityManager.h"

//Forward declarations
class Item;
class Weapon;
class Armor;

class Inventory
{
private:
    //Use a list instead of a vector, since inventories are highly mutable
    //and to allow efficient sorting.
    std::list<std::pair<Item*, int>> items;
};
