#include <cstdlib>
#include <ctime>
#include <JsonBox.h>

#include "Item.h"
#include "Weapon.h"
#include "Armor.h"
#include "EntityManager.h"

//Keeps track of items, weapons, creatures, etc
EntityManager entityManager;

int main()
{
    //Load entities
    entityManager.loadJson<Item>("Items.json");
    entityManager.loadJson<Weapon>("Weapons.json");
    entityManager.loadJson<Armor>("Armor.json");

    //Seed the rng with the system time
    std::srand(std::time(nullptr));

    return 0;
}
